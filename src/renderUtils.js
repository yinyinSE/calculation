
export default function renderUtils() {

    this.renderBasicElement = (target, elementName, elementAttr, elementInnerText) => {
        let createdElement = this.createElementForRender(elementName, elementAttr)
        if(elementInnerText) {
            createdElement.innerText = elementInnerText
        }
        target.appendChild(createdElement)
    }

    this.renderSelect = (target, elementName, elementAttr, optionList, optionShowKey) => {
        let select = this.createElementForRender(elementName, elementAttr)
        this.addOptionToSelect(select, optionList, optionShowKey)
        target.appendChild(select)
    }

    this.addOptionToSelect = (selectElement, optionList, optionShowKey) => {
        optionList.map((oneOption, index) => {
            selectElement.add(new Option(oneOption[optionShowKey], index))
        })
    }

    this.createElementForRender = (elementName, attributeObj, elementText) => {
        let element = document.createElement(elementName)
        for (let [key, value] of Object.entries(attributeObj)) {
            element.setAttribute(key, value)
        }

        if(elementText) {
            element.innerText = elementText
        }

        return element
    }

    this.renderTable = (target, list) => {
        if(list.length > 0) {
            let fragment = document.createDocumentFragment()
            list.map((item, index) => {
                let header = document.createElement('tr')
                let row = document.createElement('tr')

                for(let[key, value] of Object.entries(item)) {
                    if(index === 0){
                        let headerCell = document.createElement('th')
                        headerCell.appendChild(document.createTextNode(key))
                        header.appendChild(headerCell)
                        fragment.appendChild(header)
                    }
                    let cell = document.createElement('td')
                    cell.appendChild(document.createTextNode(value))
                    row.appendChild(cell)
                }
                fragment.appendChild(row)
            })
            target.appendChild(fragment)
        }
    }

    this.addListener = (id, listenerName, listenerFunc) => {
        document.getElementById(id).addEventListener(listenerName, listenerFunc);
    }
}


