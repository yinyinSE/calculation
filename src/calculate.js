import RenderUtils  from './renderUtils'
import { getDistanceAPI } from './include'

export default function Calculate(targetElement) {
    this.target = targetElement.target
    this.resultTableList = []
    this.shapes = [
        {type: 'circle', needDimensions: 1},
        {type: 'square', needDimensions: 1} ,
        {type: 'rectangle', needDimensions: 2},
        {type: 'ellipse', needDimensions: 2}
    ]

    const renderUtils = new RenderUtils()

    this.renderHtml = () => {
        renderUtils.renderBasicElement(this.target, 'label', {id: 'times'}, 'times')
        renderUtils.renderBasicElement(this.target, 'input', {id: 'timesInput', type: 'text'})
        renderUtils.renderSelect(this.target, 'select', {id: 'select'}, this.shapes, 'type')
        renderUtils.renderBasicElement(this.target, 'button', {id: 'calculate'}, 'Calculate')
        this.showResult(this.resultTableList, false)

        renderUtils.addListener('calculate', 'click', this.submitClickFunction)
    }

    this.submitClickFunction = () => {
        let selectIndex = document.getElementById('select').value
        let newResultObj = {
            areaType: '',
            calculationTimes: 0,
            areaResult: 0,
        }

        newResultObj.areaType = this.shapes[selectIndex].type
        newResultObj.calculationTimes = document.getElementById('timesInput').value

        if(newResultObj.calculationTimes && !isNaN(newResultObj.calculationTimes)) {

            let numberOfDimensions = this.shapes[selectIndex].needDimensions
            console.log(numberOfDimensions)

            getDistanceAPI((distance1) => {
                newResultObj.dimension1 = distance1.distance

                if(numberOfDimensions === 1) {
                    newResultObj.dimension2 = '-'

                    if(newResultObj.areaType === 'circle') {
                        newResultObj.areaResult = this.calculation(newResultObj.calculationTimes, newResultObj.dimension1, 0, this.calculateCircle)
                    } else if(newResultObj.areaType === 'square') {
                        newResultObj.areaResult = this.calculation(newResultObj.calculationTimes, newResultObj.dimension1, 0, this.calculateSquare)
                    }
                    this.showResult(this.resultTableList, true, newResultObj)

                } else if (numberOfDimensions === 2){

                    getDistanceAPI((distance2) => {
                        newResultObj.dimension2 = distance2.distance

                        if(this.shapes[selectIndex].type === 'rectangle') {
                            newResultObj.areaResult = this.calculation(newResultObj.calculationTimes, newResultObj.dimension1, newResultObj.dimension2, this.calculateRectangle)
                        } else if (this.shapes[selectIndex].type === 'ellipse') {
                            newResultObj.areaResult = this.calculation(newResultObj.calculationTimes, newResultObj.dimension1, newResultObj.dimension2, this.calculateEllipse)
                        }

                        this.showResult(this.resultTableList, true, newResultObj)
                    })
                } else {
                    alert('You need to have code deal with more dimensions calculation')
                }
            })
        } else {
            alert('Please write a number in the field!')
        }
    }

    this.showResult = (list, needRemoveOldTableList, newResultObj) => {
        if(needRemoveOldTableList) {
            document.getElementById('result').remove()
            this.setAllItemsInListToNotLatest(list)

            newResultObj.isLatest = true;
            list.push(newResultObj)
        }

        renderUtils.renderBasicElement(this.target, 'table', {id: 'result'})
        renderUtils.renderTable(document.getElementById('result'), list)
    }

    this.setAllItemsInListToNotLatest = (list) => {
        if(list.length > 0) {
            list.map((item) => {
                item.isLatest = false
            })
        }
    }

    this.calculation = (times, distance1, distance2, callback) => {
        return callback(times, distance1, distance2)
    }

    this.calculateCircle = (times, distance) => {
        return 3.14 * distance * distance * times
    }

    this.calculateSquare = (times, distance) => {
        return distance * distance * times
    }

    this.calculateRectangle = (times, distance1, distance2) => {
        return  distance1 * distance2 * times
    }

    this.calculateEllipse = (times, distance1, distance2) => {
        return 3.14 * distance1 * distance2 * times
    }
}


